
function SomeState()
{
    return {

        handler: document.getElementById("handler"),

        OnMouseDown: function() { 

         },
        OnMouseUp: function() { 
            
         },
        OnGlobalMouseUp: function() { 
            
        },
        OnMouseMove: function(mousePos) { 
            
        },
        OnMouseLeave: function() { 
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            
        },

        Exit: function() {

        }
    };
}

function BrushState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        lastMousePos: {x: 0, y: 0},

        SetTempToFillStyle: function(){
            // Composition, fill style, layer, opacity
            rgb = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
            Data.tempCtx.globalCompositeOperation = "source-over";
            Data.tempCtx.fillStyle = `rgb(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255})`;
            Data.temp.style.zIndex = parseInt(Data.canvas.style.zIndex)+1;

            Data.temp.style.opacity = Data.TS.Brush.Opacity / 255; // Restore this!!!
        },

        DrawAt: function(x, y){
            r = Data.TS.Brush.Size;
            if(Data.TS.Brush.Shape == 0)
            {
                Data.tempCtx.beginPath();
                Data.tempCtx.arc(x, y, Data.TS.Brush.Size, 0, 2*Math.PI, true);
                Data.tempCtx.fill();
            }
            else if (Data.TS.Brush.Shape == 1)
            {
                Data.tempCtx.beginPath();
                Data.tempCtx.moveTo(x-r, y-r);
                Data.tempCtx.lineTo(x-r, y+r);
                Data.tempCtx.lineTo(x+r, y+r);
                Data.tempCtx.lineTo(x+r, y-r);
                Data.tempCtx.closePath();
                Data.tempCtx.fill();
            }
            else if (Data.TS.Brush.Shape == 2)
            {
                Data.tempCtx.beginPath();
                Data.tempCtx.moveTo(x, y-r);
                Data.tempCtx.lineTo(x-1.115*r, y+0.5*r);
                Data.tempCtx.lineTo(x+1.115*r, y+0.5*r);
                Data.tempCtx.closePath();
                Data.tempCtx.fill();
            }
            
        },

        OnMouseDown: function(e) { 
            console.log("MouseDown detected.")
            Do(Data.canvas);
            this.drawing = true;
            this.SetTempToFillStyle();

            offset = $('#handler').offset();
            x = e.pageX - offset.left;
            y = e.pageY - offset.top;
            // Offset with scale
            x *= CanvasHandler.eventHandlerCV.width / CanvasHandler.eventHandlerCV.offsetWidth;
            y *= CanvasHandler.eventHandlerCV.height / CanvasHandler.eventHandlerCV.offsetHeight;
            this.DrawAt(x, y);

         },
        OnMouseUp: function() { 
            console.log("MouseUp detected.")
            this.drawing = false;
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            this.drawing = false;
            // Fill to the layer, and return temp to its initial state
            // Composition, fill style, layer, opacity
            Data.ctx.globalCompositeOperation = "source-over";
            Data.ctx.globalAlpha = Data.temp.style.opacity;
            Data.ctx.drawImage(Data.temp, 0, 0);

            Data.tempCtx.globalCompositeOperation = "copy";
            Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);


            Data.temp.style.opacity = 1;
            Data.temp.style.zIndex = '998';
            Data.ctx.globalAlpha = 1;
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                this.SetTempToFillStyle();
                // Rasterization
                var delta = {x: this.lastMousePos.x - mousePos.x, y: this.lastMousePos.y - mousePos.y};
                var dist = Math.sqrt(delta.x * delta.x + delta.y * delta.y);
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    deltaNormal = {x: delta.x / dist, y: delta.y / dist};
                    for(i = 0; i < dist; i++)
                    {
                        coord = {x: this.lastMousePos.x - i * deltaNormal.x, y: this.lastMousePos.y - i * deltaNormal.y};
                        this.DrawAt(coord.x, coord.y);
                    }

                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: Brush");
            TS_ChangeTo('TS_Brush');
        },

        Exit: function() {
            console.log("Exiting State: Brush");
        }
    };
}

function EraserState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        lastMousePos: {x: 0, y: 0},

        Prepare: async function(){
            Data.tempCtx.globalCompositeOperation = 'copy';
            Data.temp2Ctx.globalCompositeOperation = 'copy';
            Data.invsup.globalCompositeOperation = 'source-over';
            Data.temp.style.opacity = 1;
            Data.temp2.style.opacity = 1;

            Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
            Data.temp2Ctx.clearRect(0, 0, Data.temp2.width, Data.temp2.height);
            Data.invCtx.clearRect(0, 0, Data.invsup.width, Data.invsup.height);

            Data.tempCtx.drawImage(Data.canvas, 0, 0);
            Data.canvas.style.opacity = 0;
            
        },

        SetTempToFillStyle: function(){
            // Composition, fill style, layer, opacity
           /* Data.tempCtx.globalCompositeOperation = "destination-out";
            Data.tempCtx.fillStyle = `rgb(255, 255, 255)`;
            Data.temp.style.opacity = 1; // Restore this!!!
            
            Data.temp.style.zIndex = Data.canvas.style.zIndex;
            Data.temp2.style.zIndex = Data.canvas.style.zIndex+1;
            Data.temp2.style.opacity = 1 - Data.TS.Eraser.Opacity/255; // Restore this!!!
            Data.temp2Ctx.fillStyle = `rgba(255, 255, 255, ${255-Data.TS.Eraser.Opacity})`;*/
        },

        DrawAt: function(x, y){
            // Clear temp, draw temp2
            /*Data.tempCtx.beginPath();
            Data.tempCtx.arc(x, y, Data.TS.Eraser.Size, 0, 2*Math.PI, true);
            Data.tempCtx.fill();


            Data.temp2Ctx.globalCompositeOperation = 'source-over';
            //Data.temp2Ctx.clearRect(0, 0, Data.temp.width, Data.temp.height);
            //Data.temp2Ctx.drawImage(Data.invsup, 0, 0);
            Data.temp2Ctx.beginPath();
            Data.temp2Ctx.arc(x, y, Data.TS.Eraser.Size, 0, 2*Math.PI, true);
            Data.temp2Ctx.fill();
            Data.temp2Ctx.globalCompositeOperation = 'source-in';
            Data.temp2Ctx.fillStyle = 'white';
            Data.temp2Ctx.fillRect(0, 0, 640, 480);
            Data.temp2Ctx.drawImage(Data.canvas, 0, 0);*/

            r = Data.TS.Eraser.Size;
            if(Data.TS.Eraser.Shape == 0)
            {
                Data.ctx.beginPath();
                Data.ctx.arc(x, y, r, 0, 2*Math.PI, true);
                Data.ctx.fill();
            }
            else if (Data.TS.Eraser.Shape == 1)
            {
                Data.ctx.beginPath();
                Data.ctx.moveTo(x-r, y-r);
                Data.ctx.lineTo(x-r, y+r);
                Data.ctx.lineTo(x+r, y+r);
                Data.ctx.lineTo(x+r, y-r);
                Data.ctx.closePath();
                Data.ctx.fill();
            }
            else if (Data.TS.Eraser.Shape == 2)
            {
                Data.ctx.beginPath();
                Data.ctx.moveTo(x, y-r);
                Data.ctx.lineTo(x-1.115*r, y+0.5*r);
                Data.ctx.lineTo(x+1.115*r, y+0.5*r);
                Data.ctx.closePath();
                Data.ctx.fill();
            }
        },

        OnMouseDown: async function(e) { 
            console.log("MouseDown detected.");
            Do(Data.canvas);
            this.drawing = true;
            
            /*this.Prepare();
            this.SetTempToFillStyle();*/
            Data.ctx.globalAlpha = 1;
            Data.ctx.globalCompositeOperation = 'destination-out';

            offset = $('#handler').offset();
            x = e.pageX - offset.left;
            y = e.pageY - offset.top;
            // Offset with scale
            x *= CanvasHandler.eventHandlerCV.width / CanvasHandler.eventHandlerCV.offsetWidth;
            y *= CanvasHandler.eventHandlerCV.height / CanvasHandler.eventHandlerCV.offsetHeight;
            this.DrawAt(x, y);

         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            if(!this.drawing) return;
            this.drawing = false;

            // Cover with T1, then T2, and Clear&Restore both of them, and set canvas to visible
            /*Data.ctx.globalCompositeOperation = 'copy';
            Data.ctx.clearRect(0, 0, Data.canvas.width, Data.canvas.height);
            Data.ctx.globalAlpha = Data.temp2.style.opacity;
            Data.ctx.drawImage(Data.temp2, 0, 0);

            Data.ctx.globalCompositeOperation = 'source-over';
            Data.ctx.globalAlpha = 1;
            Data.ctx.drawImage(Data.temp, 0, 0);

            Data.ctx.globalAlpha = 1;
            Data.canvas.style.opacity = 1;
            Data.temp2.style.opacity = 1;
            Data.temp2Ctx.globalCompositeOperation = 'copy';
            Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
            Data.temp2Ctx.clearRect(0, 0, Data.temp2.width, Data.temp2.height);
            Data.temp.style.zIndex = 998;
            Data.temp2.style.zIndex = 997;*/
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                // Rasterization
                var delta = {x: this.lastMousePos.x - mousePos.x, y: this.lastMousePos.y - mousePos.y};
                var dist = Math.sqrt(delta.x * delta.x + delta.y * delta.y);
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    deltaNormal = {x: delta.x / dist, y: delta.y / dist};
                    for(i = 0; i < dist; i++)
                    {
                        coord = {x: this.lastMousePos.x - i * deltaNormal.x, y: this.lastMousePos.y - i * deltaNormal.y};
                        this.DrawAt(coord.x, coord.y);
                    }
                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: Eraser");
            TS_ChangeTo('TS_Eraser');
        },

        Exit: function() {
            console.log("Exiting State: Eraser");
        }
    };
}


function DrawLineState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        holdUpPos: {x:0, y:0},
        lastMousePos: {x: 0, y: 0},

        

        OnMouseDown: function() { 
            console.log("MouseDown detected.");
            Do(Data.canvas);
            this.holdUpPos.x = this.lastMousePos.x;
            this.holdUpPos.y = this.lastMousePos.y;
            this.drawing = true;
            Data.temp.style.zIndex = parseInt(Data.canvas.style.zIndex)+1;
            // settings of temp
            
         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            if(this.drawing)
            {
                // if the user was drawing, copy this image to the selected canvas and then clear it
                Data.ctx.globalCompositeOperation = "source-over";
                Data.ctx.drawImage(Data.temp, 0, 0);

                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
                Data.temp.style.zIndex = '998';
            }
            this.drawing = false;
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                Data.tempCtx.globalCompositeOperation = "copy";
                rgb = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
                Data.tempCtx.strokeStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Line.Opacity/255})`;
                Data.tempCtx.lineWidth = Data.TS.Line.Width;
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    Data.tempCtx.beginPath();
                    Data.tempCtx.moveTo(this.holdUpPos.x, this.holdUpPos.y);
                    Data.tempCtx.lineTo(mousePos.x, mousePos.y);
                    Data.tempCtx.stroke();
                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: DrawLine");
            TS_ChangeTo("TS_Line");
        },

        Exit: function() {
            console.log("Exiting State: DrawLine");
        }
    };
}

function DrawCircleState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        holdUpPos: {x:0, y:0},
        lastMousePos: {x: 0, y: 0},

        OnMouseDown: function() { 
            console.log("MouseDown detected.");
            Do(Data.canvas);
            this.holdUpPos.x = this.lastMousePos.x;
            this.holdUpPos.y = this.lastMousePos.y;
            this.drawing = true;
            Data.temp.style.zIndex = parseInt(Data.canvas.style.zIndex)+1;
         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            if(this.drawing)
            {
                // if the user was drawing, copy this image to the selected canvas and then clear it
                Data.ctx.globalCompositeOperation = "source-over";
                Data.ctx.drawImage(Data.temp, 0, 0);

                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
                Data.temp.style.zIndex = '998';
            }
            this.drawing = false;
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                Data.tempCtx.globalCompositeOperation = "copy";
                rgb = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
                Data.tempCtx.strokeStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Circle.Opacity/255})`;
                Data.tempCtx.fillStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Circle.Opacity/255})`;
                Data.tempCtx.lineWidth = Data.TS.Circle.Width;
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    Data.tempCtx.beginPath();
                    r = Math.sqrt((this.holdUpPos.x - mousePos.x) * (this.holdUpPos.x - mousePos.x) + (this.holdUpPos.y - mousePos.y) * (this.holdUpPos.y - mousePos.y));
                    Data.tempCtx.arc(this.holdUpPos.x, this.holdUpPos.y, r, 0, Math.PI*2, true);
                    if(Data.TS.Circle.Filled) 
                        Data.tempCtx.fill();
                    else 
                        Data.tempCtx.stroke();
                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: DrawCircle");
            TS_ChangeTo("TS_Circle");
        },

        Exit: function() {
            console.log("Exiting State: DrawCircle");
        }
    };
}

function DrawSquareState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        holdUpPos: {x:0, y:0},
        lastMousePos: {x: 0, y: 0},

        OnMouseDown: function() { 
            console.log("MouseDown detected.");
            Do(Data.canvas);
            this.holdUpPos.x = this.lastMousePos.x;
            this.holdUpPos.y = this.lastMousePos.y;
            this.drawing = true;
            Data.temp.style.zIndex = parseInt(Data.canvas.style.zIndex)+1;
         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            if(this.drawing)
            {
                // if the user was drawing, copy this image to the selected canvas and then clear it
                Data.ctx.globalCompositeOperation = "source-over";
                Data.ctx.drawImage(Data.temp, 0, 0);

                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
                Data.temp.style.zIndex = '998';
            }
            this.drawing = false;
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.lineWidth = Data.TS.Square.Width;
                rgb = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
                Data.tempCtx.strokeStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Square.Opacity/255})`;
                Data.tempCtx.fillStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Square.Opacity/255})`;
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    Data.tempCtx.beginPath();
                    Data.tempCtx.rect(
                        Math.min(this.holdUpPos.x, mousePos.x), 
                        Math.min(this.holdUpPos.y, mousePos.y),
                        Math.abs(this.holdUpPos.x - mousePos.x),
                        Math.abs(this.holdUpPos.y - mousePos.y) );
                    if(Data.TS.Square.Filled) 
                        Data.tempCtx.fill();
                    else 
                        Data.tempCtx.stroke();
                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: DrawSquare");
            TS_ChangeTo("TS_Square");
        },

        Exit: function() {
            console.log("Exiting State: DrawSquare");
        }
    };
}

function DrawTriangleState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        holdUpPos: {x:0, y:0},
        lastMousePos: {x: 0, y: 0},

        OnMouseDown: function() { 
            console.log("MouseDown detected.");
            Do(Data.canvas);
            this.holdUpPos.x = this.lastMousePos.x;
            this.holdUpPos.y = this.lastMousePos.y;
            this.drawing = true;
            Data.temp.style.zIndex = parseInt(Data.canvas.style.zIndex)+1;
         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            if(this.drawing)
            {
                // if the user was drawing, copy this image to the selected canvas and then clear it
                Data.ctx.globalCompositeOperation = "source-over";
                Data.ctx.drawImage(Data.temp, 0, 0);

                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
                Data.temp.style.zIndex = '998';
            }
            this.drawing = false;
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.lineWidth = Data.TS.Triangle.Width;
                rgb = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
                Data.tempCtx.strokeStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Triangle.Opacity/255})`;
                Data.tempCtx.fillStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Triangle.Opacity/255})`;
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    Data.tempCtx.beginPath();
                    
                    // Find p2 and p3
                    p0 = {x: this.holdUpPos.x, y: this.holdUpPos.y};
                    p1 = {x: mousePos.x, y: mousePos.y};
                    p2 = {x: (1.5 * p0.x - 0.5 * p1.x + 0.866 * p0.y - 0.866 * p1.y), y: (0.866 * p1.x - 0.866 * p0.x + 1.5 * p0.y - 0.5 * p1.y)};
                    p3 = {x: (1.5 * p0.x - 0.5 * p1.x - 0.866 * p0.y + 0.866 * p1.y), y: (0.866 * p0.x - 0.866 * p1.x + 1.5 * p0.y - 0.5 * p1.y)};
                    Data.tempCtx.moveTo(p1.x, p1.y);
                    Data.tempCtx.lineTo(p2.x, p2.y);
                    Data.tempCtx.lineTo(p3.x, p3.y);
                    Data.tempCtx.closePath();
                    if(Data.TS.Triangle.Filled) 
                        Data.tempCtx.fill();
                    else 
                        Data.tempCtx.stroke();
                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: DrawTriangle");
            TS_ChangeTo("TS_Triangle");
        },

        Exit: function() {
            console.log("Exiting State: DrawTriangle");
        }
    };
}

function MoveLayerState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        holdUpPos: {x:0, y:0},
        lastMousePos: {x: 0, y: 0},

        OnMouseDown: function() { 
            console.log("MouseDown detected.");
            Do(Data.canvas);
            this.holdUpPos.x = this.lastMousePos.x;
            this.holdUpPos.y = this.lastMousePos.y;
            this.drawing = true;
            Data.temp.style.zIndex = parseInt(Data.canvas.style.zIndex)+1;
            Data.tempCtx.globalCompositeOperation = "copy";
            Data.tempCtx.globalAlpha = 1;
            Data.tempCtx.drawImage(Data.canvas, 0, 0);
            // Use temp to draw.
            Data.canvas.style.opacity = 0;
         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            if(this.drawing)
            {
                // if the user was drawing, copy this image to the selected canvas and then clear it
                Data.ctx.globalCompositeOperation = "copy";
                Data.ctx.drawImage(Data.temp, 0, 0);
                Data.ctx.globalCompositeOperation = "source-over";

                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
                Data.temp.style.zIndex = '998';
                Data.canvas.style.opacity = 1;
            }
            this.drawing = false;
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.globalAlpha = 1;
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    Data.tempCtx.drawImage(Data.canvas, mousePos.x - this.holdUpPos.x, mousePos.y - this.holdUpPos.y);
                    
                    
                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: MoveLayer");
        },

        Exit: function() {
            console.log("Exiting State: MoveLayer");
        }
    };
}

function RotateLayerState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        holdUpPos: {x:0, y:0},
        lastMousePos: {x: 0, y: 0},

        OnMouseDown: function() { 
            console.log("MouseDown detected.");
            Do(Data.canvas);
            this.holdUpPos.x = this.lastMousePos.x;
            this.holdUpPos.y = this.lastMousePos.y;
            this.drawing = true;
            Data.temp.style.zIndex = parseInt(Data.canvas.style.zIndex)+1;
            Data.tempCtx.globalCompositeOperation = "copy";
            Data.tempCtx.globalAlpha = 1;
            Data.tempCtx.drawImage(Data.canvas, 0, 0);
            // Use temp to draw.
            Data.canvas.style.opacity = 0;
         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            if(this.drawing)
            {
                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.save();
                Data.tempCtx.translate(this.holdUpPos.x, this.holdUpPos.y);
                rotationAmout = Math.sqrt((this.lastMousePos.x - this.holdUpPos.x) * (this.lastMousePos.x - this.holdUpPos.x) + (this.lastMousePos.y - this.holdUpPos.y) * (this.lastMousePos.y - this.holdUpPos.y)) / 60;
                Data.tempCtx.rotate(rotationAmout);
                Data.tempCtx.translate(-this.holdUpPos.x, -this.holdUpPos.y);
                Data.tempCtx.drawImage(Data.canvas, 0, 0);
                Data.tempCtx.restore();
                // if the user was drawing, copy this image to the selected canvas and then clear it
                Data.ctx.globalCompositeOperation = "copy";
                Data.ctx.drawImage(Data.temp, 0, 0);
                Data.ctx.globalCompositeOperation = "source-over";

                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
                Data.temp.style.zIndex = '998';
                Data.canvas.style.opacity = 1;
            }
            this.drawing = false;
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.globalAlpha = 1;
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    // Use canvas status stack to draw
                    Data.tempCtx.save();
                    Data.tempCtx.translate(this.holdUpPos.x, this.holdUpPos.y);
                    rotationAmout = Math.sqrt((mousePos.x - this.holdUpPos.x) * (mousePos.x - this.holdUpPos.x) + (mousePos.y - this.holdUpPos.y) * (mousePos.y - this.holdUpPos.y)) / 60;
                    Data.tempCtx.rotate(rotationAmout);
                    Data.tempCtx.translate(-this.holdUpPos.x, -this.holdUpPos.y);
                    Data.tempCtx.drawImage(Data.canvas, 0, 0);
                    Data.tempCtx.restore();
                    Data.tempCtx.globalCompositeOperation = "source-over";
                    // Draw a indication line
                    Data.tempCtx.beginPath();
                    Data.tempCtx.arc(this.holdUpPos.x, this.holdUpPos.y, 5, 0, Math.PI * 2, true);
                    Data.tempCtx.fillStyle = 'red';
                    Data.tempCtx.fill();
                    Data.tempCtx.beginPath();
                    Data.tempCtx.arc(this.holdUpPos.x, this.holdUpPos.y, 3, 0, Math.PI * 2, true);
                    Data.tempCtx.fillStyle = 'black';
                    Data.tempCtx.fill();

                    Data.tempCtx.beginPath();
                    Data.tempCtx.arc(mousePos.x, mousePos.y, 5, 0, Math.PI * 2, true);
                    Data.tempCtx.fillStyle = 'red';
                    Data.tempCtx.fill();
                    Data.tempCtx.beginPath();
                    Data.tempCtx.arc(mousePos.x, mousePos.y, 3, 0, Math.PI * 2, true);
                    Data.tempCtx.fillStyle = 'black';
                    Data.tempCtx.fill();

                    Data.tempCtx.beginPath();
                    Data.tempCtx.moveTo(this.holdUpPos.x, this.holdUpPos.y);
                    Data.tempCtx.lineTo(mousePos.x, mousePos.y);
                    Data.tempCtx.lineWidth = 3;
                    Data.tempCtx.strokeStyle = 'red';
                    Data.tempCtx.stroke();
                    
                    Data.tempCtx.beginPath();
                    Data.tempCtx.moveTo(this.holdUpPos.x, this.holdUpPos.y);
                    Data.tempCtx.lineTo(mousePos.x, mousePos.y);
                    Data.tempCtx.lineWidth = 1;
                    Data.tempCtx.strokeStyle = 'black';
                    Data.tempCtx.stroke();

                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: Rotate");
        },

        Exit: function() {
            console.log("Exiting State: Rotate");
        }
    };
}

function ScaleLayerState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        holdUpPos: {x:0, y:0},
        lastMousePos: {x: 0, y: 0},

        OnMouseDown: function() { 
            console.log("MouseDown detected.");
            Do(Data.canvas);
            this.holdUpPos.x = this.lastMousePos.x;
            this.holdUpPos.y = this.lastMousePos.y;
            this.drawing = true;
            Data.temp.style.zIndex = parseInt(Data.canvas.style.zIndex)+1;
            Data.tempCtx.globalCompositeOperation = "copy";
            Data.tempCtx.globalAlpha = 1;
            Data.tempCtx.drawImage(Data.canvas, 0, 0);
            // Use temp to draw.
            Data.canvas.style.opacity = 0;
         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            if(this.drawing)
            {
                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.save();
                Data.tempCtx.translate(this.holdUpPos.x, this.holdUpPos.y);
                scaleAmount = {x: (this.lastMousePos.x - this.holdUpPos.x)/250, y:(this.lastMousePos.y - this.holdUpPos.y)/250};
                if(Data.TS.Scale.KAR) scaleAmount = {x: (scaleAmount.x + scaleAmount.y)/2, y: (scaleAmount.x + scaleAmount.y)/2};
                Data.tempCtx.scale(scaleAmount.x, scaleAmount.y);
                Data.tempCtx.translate(-this.holdUpPos.x, -this.holdUpPos.y);
                Data.tempCtx.drawImage(Data.canvas, 0, 0);
                Data.tempCtx.restore();
                // if the user was drawing, copy this image to the selected canvas and then clear it
                Data.ctx.globalCompositeOperation = "copy";
                Data.ctx.drawImage(Data.temp, 0, 0);
                Data.ctx.globalCompositeOperation = "source-over";

                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.clearRect(0, 0, Data.temp.width, Data.temp.height);
                Data.temp.style.zIndex = '998';
                Data.canvas.style.opacity = 1;
            }
            this.drawing = false;
         },
        OnMouseMove: async function(mousePos) { 
            if(this.drawing)
            {
                if(this.inProcess) return;
                Data.tempCtx.globalCompositeOperation = "copy";
                Data.tempCtx.globalAlpha = 1;
                // Wait until all path is drawn
                await new Promise((resolve, reject) => {
                    this.inProcess = true;
                    // Use canvas status stack to draw
                    Data.tempCtx.save();
                    Data.tempCtx.translate(this.holdUpPos.x, this.holdUpPos.y);
                    scaleAmount = {x: (mousePos.x - this.holdUpPos.x)/250, y:(mousePos.y - this.holdUpPos.y)/250};
                    if(Data.TS.Scale.KAR) scaleAmount = {x: (scaleAmount.x + scaleAmount.y)/2, y: (scaleAmount.x + scaleAmount.y)/2};
                    Data.tempCtx.scale(scaleAmount.x, scaleAmount.y);
                    Data.tempCtx.translate(-this.holdUpPos.x, -this.holdUpPos.y);
                    Data.tempCtx.drawImage(Data.canvas, 0, 0);
                    Data.tempCtx.restore();
                    Data.tempCtx.globalCompositeOperation = "source-over";
                    // Draw a indication line
                    Data.tempCtx.beginPath();
                    Data.tempCtx.arc(this.holdUpPos.x, this.holdUpPos.y, 5, 0, Math.PI * 2, true);
                    Data.tempCtx.fillStyle = 'red';
                    Data.tempCtx.fill();
                    Data.tempCtx.beginPath();
                    Data.tempCtx.arc(this.holdUpPos.x, this.holdUpPos.y, 3, 0, Math.PI * 2, true);
                    Data.tempCtx.fillStyle = 'black';
                    Data.tempCtx.fill();

                    Data.tempCtx.beginPath();
                    Data.tempCtx.arc(mousePos.x, mousePos.y, 5, 0, Math.PI * 2, true);
                    Data.tempCtx.fillStyle = 'red';
                    Data.tempCtx.fill();
                    Data.tempCtx.beginPath();
                    Data.tempCtx.arc(mousePos.x, mousePos.y, 3, 0, Math.PI * 2, true);
                    Data.tempCtx.fillStyle = 'black';
                    Data.tempCtx.fill();

                    Data.tempCtx.beginPath();
                    Data.tempCtx.moveTo(this.holdUpPos.x, this.holdUpPos.y);
                    Data.tempCtx.lineTo(this.holdUpPos.x, mousePos.y);
                    Data.tempCtx.lineTo(mousePos.x, mousePos.y);
                    Data.tempCtx.lineTo(mousePos.x, this.holdUpPos.y);
                    Data.tempCtx.closePath();
                    Data.tempCtx.lineWidth = 3;
                    Data.tempCtx.strokeStyle = 'red';
                    Data.tempCtx.stroke();
                    
                    Data.tempCtx.beginPath();
                    Data.tempCtx.moveTo(this.holdUpPos.x, this.holdUpPos.y);
                    Data.tempCtx.lineTo(this.holdUpPos.x, mousePos.y);
                    Data.tempCtx.lineTo(mousePos.x, mousePos.y);
                    Data.tempCtx.lineTo(mousePos.x, this.holdUpPos.y);
                    Data.tempCtx.closePath();
                    Data.tempCtx.lineWidth = 1;
                    Data.tempCtx.strokeStyle = 'black';
                    Data.tempCtx.stroke();

                    resolve();
                })
                .then(() => {
                    this.inProcess = false;
                });
                
            }
            
            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: ScaleLayerState");
            TS_ChangeTo("TS_Scale");
        },

        Exit: function() {
            console.log("Exiting State: ScaleLayerState");
        }
    };
}

function TextState(){
    return {
        handler: document.getElementById("handler"),
        drawing: false,
        inProcess: false,
        holdUpPos: {x:0, y:0},
        lastMousePos: {x: 0, y: 0},

        OnMouseDown: function(e) { 
            console.log("MouseDown detected.");
            //text = prompt('Please enter text.\nThe text will be on the left-top corner; \nYou can use translation tools to edit it later.');
            //if(text == null || text.length == 0) return;
            text = document.getElementById("TextInput").value;
            Do(Data.canvas);

            var offset = $('#handler').offset();
            x = (e.pageX - offset.left);
            y = (e.pageY - offset.top);
            // Offset with scale
            x *= CanvasHandler.eventHandlerCV.width / CanvasHandler.eventHandlerCV.offsetWidth;
            y *= CanvasHandler.eventHandlerCV.height / CanvasHandler.eventHandlerCV.offsetHeight;

            Data.ctx.font = `${Data.TS.Text.Bold ? 'bold ' : ''}${Data.TS.Text.Italic ? 'italic ' : ''}${Data.TS.Text.Size}px ${Data.TS.Text.Font} `;
            Data.ctx.globalAlpha = 1;
            Data.ctx.globalCompositeOperation = 'source-over';
            rgb = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
            Data.ctx.fillStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Text.Opacity/255})`;
            Data.ctx.fillText(text, x, y);
         },
        OnMouseUp: function() { 
            
         },
         OnGlobalMouseUp: function() { 
            console.log("MouseUp detected.")
            
         },
        OnMouseMove: async function(mousePos) { 
            Data.tempCtx.font = `${Data.TS.Text.Bold ? 'bold ' : ''}${Data.TS.Text.Italic ? 'italic ' : ''}${Data.TS.Text.Size}px ${Data.TS.Text.Font} `;
            Data.tempCtx.globalAlpha = Data.TS.Text.Opacity / 255;
            Data.tempCtx.globalCompositeOperation = 'copy';
            rgb = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
            Data.tempCtx.fillStyle = `rgba(${rgb.r*255}, ${rgb.g*255}, ${rgb.b*255}, ${Data.TS.Text.Opacity/255})`;
            text = document.getElementById("TextInput").value;
            Data.tempCtx.fillText(text, mousePos.x, mousePos.y);

            this.lastMousePos.x = mousePos.x;
            this.lastMousePos.y = mousePos.y;
        },
        OnMouseLeave: function(){
            
        },
        OnEvent: function(e) { 
            
        },
        // Custom Events

        Init: function() {
            console.log("Entering State: ScaleLayerState");
            TS_ChangeTo("TS_Text");
        },

        Exit: function() {
            console.log("Exiting State: ScaleLayerState");
        }
    };
}
function CreateStateManager(initialState)
{
    return {
        state: initialState,

        OnMouseDown: function(e) { this.state.OnMouseDown(e); },
        OnMouseUp: function(e) { this.state.OnMouseUp(e); },
        OnGlobalMouseUp: function(e) { this.state.OnGlobalMouseUp(e); },
        OnMouseMove: function(mousePos) { this.state.OnMouseMove(mousePos); },
        OnMouseLeave: function(e) { this.state.OnMouseLeave(e); },
        OnEvent: function(e) { this.state.OnEvent(e); },

        TransferTo: function(s) {
            this.state.Exit();
            this.state = s;
            this.state.Init();
        },
    };
}
/*
    State:  OnMouseDown
            OnMouseUp
            OnMouseMove(mousePos)
            OnEvent
            Init
            Exit
*/

function CreateCanvasHandler(elementID, initialState)
{

    var  handler = {
        // The top-most canvas for event-handling
        eventHandlerCV: document.getElementById("handler"),
        // The position of the mouse
        mousePos : { x: 0, y: 0 },
        // Currently selected canvas
        cv: document.getElementById("base"),
        // Current Context

        // The state manager
        stateManager: CreateStateManager(initialState),

        // The on-mouse-move event
        OnMouseMove: function (e) {
            this.mousePos.x = e.pageX;
            this.mousePos.y = e.pageY;

            var offset = $('#handler').offset();
            var x = this.mousePos.x - offset.left;
            var y = this.mousePos.y - offset.top;
            // Offset with scale
            x *= CanvasHandler.eventHandlerCV.width / CanvasHandler.eventHandlerCV.offsetWidth;
            y *= CanvasHandler.eventHandlerCV.height / CanvasHandler.eventHandlerCV.offsetHeight;

            this.stateManager.OnMouseMove({x: x, y: y});
        },

        OnMouseDown: function(e) {
            this.stateManager.OnMouseDown(e);
        },

        OnMouseUp: function(e) {
            this.stateManager.OnMouseUp(e);
        },

        OnGlobalMouseUp: function(e) {
            this.stateManager.OnGlobalMouseUp(e);
        },

        OnMouseLeave: function(e) {
            this.stateManager.OnMouseLeave(e);
        },

        ChangeCursorTo: function(cursor){

        },

        TransferTo: function(s) {
            this.stateManager.TransferTo(s);
        }
    };

    // Register events
    var el = '#' + elementID;
    $(document).mousemove(function(event){
        handler.OnMouseMove(event);
        DrawCursor(event);
    });
    $(el).mousedown(function(e){
        handler.OnMouseDown(e);
    });
    $(el).mouseup(function(e){
        handler.OnMouseUp(e);
    });
    $(document).mouseup(function(e){
        handler.OnGlobalMouseUp(e);
    });
    $(el).mouseleave(function(e){
        handler.OnMouseLeave(e);
    });

    return handler;
}

XX=0; 
YY=0;
RR=0;
function DrawCursor(e)
{
    offset = $('#handler').offset();
    x = e.pageX - offset.left;
    y = e.pageY - offset.top;
    // Offset with scale
    x *= CanvasHandler.eventHandlerCV.width / CanvasHandler.eventHandlerCV.offsetWidth;
    y *= CanvasHandler.eventHandlerCV.height / CanvasHandler.eventHandlerCV.offsetHeight;    


    function CircleOf(r, shape = 0)
    {
        if(Data.selectedTool == 1)       
            rad = parseInt(Data.TS.Brush.Size);
        else if(Data.selectedTool == 2) 
            rad = parseInt(Data.TS.Eraser.Size);

        if(shape == 0)
        {
            Data.cursorCtx.beginPath();
            Data.cursorCtx.arc(x, y, rad, 0, 2*Math.PI, true);
            Data.cursorCtx.stroke();
        }
        else if (shape == 1)
        {
            Data.cursorCtx.beginPath();
            Data.cursorCtx.moveTo(x-rad, y-rad);
            Data.cursorCtx.lineTo(x-rad, y+rad);
            Data.cursorCtx.lineTo(x+rad, y+rad);
            Data.cursorCtx.lineTo(x+rad, y-rad);
            Data.cursorCtx.closePath();
            Data.cursorCtx.stroke();
        }
        else if (shape == 2)
        {
            Data.cursorCtx.beginPath();
            Data.cursorCtx.moveTo(x, y-rad);
            Data.cursorCtx.lineTo(x-1.115*rad, y+0.5*rad);
            Data.cursorCtx.lineTo(x+1.115*rad, y+0.5*rad);
            Data.cursorCtx.closePath();
            Data.cursorCtx.stroke();
        }
    }
    function Cross()
    {
        Data.cursorCtx.fillRect(x-8, y, 16, 1);
        Data.cursorCtx.fillRect(x, y-8, 1, 16);
    }
    // Draw
    Data.cursorCtx.strokeStyle = 'black';
    Data.cursorCtx.globalCompositeOperation = 'copy';
    //Data.cursorCtx.clearRect(0, 0, Data.cursor.width, Data.cursor.height);
    if(Data.selectedTool == 1)       
        CircleOf(Data.TS.Brush.Size, Data.TS.Brush.Shape);
    else if(Data.selectedTool == 2)  
        CircleOf(Data.TS.Eraser.Size, Data.TS.Eraser.Shape);
    else if(Data.selectedTool >= 3 && Data.selectedTool <= 6)  
        Cross();
    else if(Data.selectedTool >= 8 && Data.selectedTool <= 10)  
        Cross();

    XX=x;
    YY=y;
    RR=r;
}
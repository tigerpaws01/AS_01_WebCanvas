var CanvasHandler;
var Data = {
    OnToolSelected: [
        null,
        // Brush
        function(){
            CanvasHandler.TransferTo(BrushState());
        },
        // Eraser
        function(){
            CanvasHandler.TransferTo(EraserState());
        },
        // DrawLine
        function(){
            CanvasHandler.TransferTo(DrawLineState());
        },
        // DrawCircle
        function(){
            CanvasHandler.TransferTo(DrawCircleState());
        },
        // DrawSquare
        function(){
            CanvasHandler.TransferTo(DrawSquareState());
        },
        // DrawTriangle
        function(){
            CanvasHandler.TransferTo(DrawTriangleState());
        },
        // Text
        function(){
            CanvasHandler.TransferTo(TextState());
        },
        // Move
        function(){
            CanvasHandler.TransferTo(MoveLayerState());
        },
        // Rotate
        function(){
            CanvasHandler.TransferTo(RotateLayerState());
            
        },
        // Scale
        function(){
            CanvasHandler.TransferTo(ScaleLayerState());
        },
        // Nothing
        function(){
            
        },
        // Upload
        function(lastSelected){
            document.getElementById("imageUploader").click();
            Data.Select(lastSelected);
        },
        // Download
        function(lastSelected){
            /// create an "off-screen" anchor tag
            var lnk = document.createElement('a'), e;
            
            /// the key here is to set the download attribute of the a tag
            lnk.download = 'image';
            
            /// convert canvas content to data-uri for link. When download
            /// attribute is set the content pointed to by link will be
            /// pushed as "download" in HTML5 capable browsers
            nc = document.createElement('canvas');
            nc.className = 'fit-width';
            nc.width = 640;
            nc.height = 480;
            ncCtx = nc.getContext('2d');
            // Draw all layers on it
            for(i = 0; i < Data.layers.length; i++)
            {
                ncCtx.drawImage(Data.layers[i], 0, 0);
            }
            lnk.href = nc.toDataURL("image/png;base64");
            
            /// create a "fake" click-event to trigger the download
            if (document.createEvent) {
                e = document.createEvent("MouseEvents");
                e.initMouseEvent("click", true, true, window,
                                0, 0, 0, 0, 0, false, false, false,
                                false, 0, null);
            
                lnk.dispatchEvent(e);
            } else if (lnk.fireEvent) {
                lnk.fireEvent("onclick");
            }
            Data.Select(lastSelected);
            delete nc;
        },
        // Undo
        function(lastSelected){
            Undo();
            Data.Select(lastSelected);
        },
        // Redo
        function(lastSelected){
            Redo();
            Data.Select(lastSelected);
        },
        // Reset
        function(lastSelected){
            Reset();
            Data.Select(lastSelected);
        },
    ],

    Select: function(index)
    {
        lastSelected = this.selectedTool;
        this.selectedTool = index;
        this.OnToolSelected[index](lastSelected);
    },

    TS: {
        Brush:{
            Size: 10,
            Opacity: 255,
            Shape: 0
        },
        Eraser:{
            Size: 10,
            Shape: 0,
        },
        Line:{
            Width:1,
            Opacity: 255,
        },
        Square:{
            Width:1,
            Opacity: 255,
            Filled:false,
        },
        Circle:{
            Width:1,
            Opacity: 255,
            Filled:false,
        },
        Triangle:{
            Width:1,
            Opacity: 255,
            Filled:false,
        },
        Text:{
            Font:"Arial",
            Size:20,
            Opacity:255,
            Bold:false,
            Italic:false,
        },
        Scale:{
            KAR:true
        }
    },

    layers:[],
    selectedLayer:0,
    layersCreated: 0,
    undoList:[],
    undoIdx:0,
};


function init()
{
    Data.temp = document.getElementById('temp');
    Data.tempCtx = Data.temp.getContext('2d');

    Data.temp2 = document.getElementById('temp2');
    Data.temp2Ctx = Data.temp2.getContext('2d');

    Data.invsup = document.getElementById('invsup');
    Data.invCtx = Data.invsup.getContext('2d');

    Data.canvas=document.getElementById('base');
    Data.ctx = Data.canvas.getContext('2d');
    Data.ctx.fillStyle = '#FFFFFF';
    Data.ctx.fillRect(0, 0, 640, 480);
    Data.ctx.fillStyle = '#000000';
    opCv = document.getElementById("opacityBase");
    opCvCtx = opCv.getContext("2d");
    for(i = 0; i < 64; i++)
        for(j = 0; j < 48; j++)
        {
            opCvCtx.fillStyle = ((i+j) % 2 == 0) ? 'rgb(255, 255, 255)' : 'rgb(200, 200, 200)';
            opCvCtx.fillRect(i*10, j*10, 10, 10);
        }

    Data.selectedTool = 1;
    
    CanvasHandler = CreateCanvasHandler("handler", BrushState());
    document.getElementById('handler').style.cursor = 'none';

    Data.cursor = document.getElementById('mouseCursor');
    Data.cursorCtx = Data.cursor.getContext('2d');

    Data.layers[0] = Data.canvas;

    ToolSetup();
    ColorPickerInit()
}

function Reset()
{
    for(i = 1; i < Data.layers.length; i++)
    {
        ctx = Data.layers[i].getContext('2d');
        ctx.globalCompositeOperation = 'source-over';
        ctx.globalAlpha = 1;
        ctx.clearRect(0, 0, Data.canvas.width, Data.canvas.height);
    }
    ctx = Data.layers[0].getContext('2d');
    ctx.globalCompositeOperation = 'source-over';
    ctx.globalAlpha = 1;
    ctx.fillStyle = 'white';
    ctx.clearRect(0, 0, Data.canvas.width, Data.canvas.height);
    ctx.fillRect(0, 0, Data.canvas.width, Data.canvas.height);
    Data.undoIdx = 0;
    if(Data.undoList.length > 0) Data.undoList.splice(0);
    ToolUpdate();
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function UploadImage(e)
{
    console.log(e);
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            NewLayer();
            totalRatio = 1;
            if(parseInt(img.width) > 640)
            {
                ratio = 640.0 / parseInt(img.width);
                totalRatio *= ratio;
                img.width = 640;
                img.height = parseInt(img.height) * ratio;
            }
            if(parseInt(img.height) > 480)
            {
                ratio = 480.0 / parseInt(img.height);
                totalRatio *= ratio;
                img.height = 480;
                img.width = parseInt(img.width) * ratio;
            }
            Data.ctx.save();
            Data.ctx.scale(totalRatio, totalRatio);
            Data.ctx.drawImage(img, 0, 0);
            Data.ctx.restore();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.files[0]);
}
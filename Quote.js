quotes = ['The Way Get Started Is To Quit Talking And Begin Doing.', 'The Pessimist Sees Difficulty In Every Opportunity. The Optimist Sees Opportunity In Every Difficulty.', 'Don\'t Let Yesterday Take Up Too Much Of Today.',
    'You Learn More From Failure Than From Success. Don\'t Let It Stop You. Failure Builds Character.', 'It\'s Not Whether You Get Knocked Down, It\'s Whether You Get Up.', 'If You Are Working On Something That You Really Care About, You Don\'t Have To Be Pushed. The Vision Pulls You.', 'People Who Are Crazy Enough To Think They Can Change The World, Are The Ones Who Do.', 'Failure Will Never Overtake Me If My Determination To Succeed Is Strong Enough.',
    'Entrepreneurs Are Great At Dealing With Uncertainty And Also Very Good At Minimizing Risk. That\'s The Classic Entrepreneur.', 'We May Encounter Many Defeats But We Must Not Be Defeated.', 'Knowing Is Not Enough; We Must Apply. Wishing Is Not Enough; We Must Do.',
    'Imagine Your Life Is Perfect In Every Respect; What Would It Look Like?', 'We Generate Fears While We Sit. We Overcome Them By Action.', 'Whether You Think You Can Or Think You Can\'t, You\'re Right.', 'Security Is Mostly A Superstition. Life Is Either A Daring Adventure Or Nothing.',
    'Yum!', 'Abracadabra.', '#$!D&^@TF@*(@@B@*((%%)(!!??'];

author = ['Walt Disney', 'Winston Churchill', 'Will Rogers', 'Unknown', 'Vince Lombardi', 'Steve Jobs', 'Rob Siltanen', 'Og Mandino', 'Mohnish Pabrai', 'Maya Angelou', 'Johann Wolfgang Von Goethe (long af)', 'Brian Tracy', 'Dr. Henry Link', 'Henry Ford', 'Helen Keller', 'A Friend Called Big-Eater', 'Some Random Magician', 'An Ancient Primitive with Wisdom'];
quoted = 0;
function GenerateRandomQuote()
{
    quoted++;
    k = Math.floor(Math.random() * (quoted > 3 ? 18 : 15));
    document.getElementById("QuoteContainer").innerHTML = 
    `<div style='margin: 15px'>
        <font size='5' face='New Times Roman'>
            <i>${quotes[k]}</i>
        </font>
    </div>
    <div style='text-align: center; margin: 15px'>
        <font size='6' face='Georgia'>
            <i>${author[k]}</i>
        </font>
    </div>`;
}
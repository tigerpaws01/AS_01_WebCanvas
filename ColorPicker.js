var ColorPickerInfo = {
    h:0,
    s:1,
    v:0,
    color:"",
    CV: null,
    Ctx: null,
    ColorPad: null,
    Rval: null,
    Gval: null,
    Bval: null,
};
// TODO: Indicator

function ColorPickerInit()
{
    ColorPickerInfo.CV = document.getElementById("ColorPicker");
    ColorPickerInfo.Ctx = ColorPickerInfo.CV.getContext('2d');
    ColorPickerInfo.Pad = document.getElementById("ColorPickerPad");
    ColorPickerInfo.Rval = document.getElementById("ColorR");
    ColorPickerInfo.Gval = document.getElementById("ColorG");
    ColorPickerInfo.Bval = document.getElementById("ColorB");
    Redraw();
    UpdateRGB();

    // Bind Mouse Events
    function mmHandler_r(e)
    {
        y = e.pageY - $('#ColorPicker').offset().top;
        ColorPickerInfo.h = Clamp((y / ColorPickerInfo.CV.height) * 360, 0, 359);
        Redraw();
    }

    function mmHandler_l(e)
    {
        x = e.pageX - $('#ColorPicker').offset().left;
        y = e.pageY - $('#ColorPicker').offset().top;
        ColorPickerInfo.s = Clamp(x / (20 / 24 * ColorPickerInfo.CV.width), 0, 1);
        ColorPickerInfo.v = Clamp(1 - (y / ColorPickerInfo.CV.height), 0, 1);
        Redraw();
    }

    $('#ColorPicker').mousedown(function(e){
        x = e.pageX - $('#ColorPicker').offset().left;
        y = e.pageY - $('#ColorPicker').offset().top;

        if(x >= ColorPickerInfo.CV.width * 21 / 24)
        {
            ColorPickerInfo.h = Clamp((y / ColorPickerInfo.CV.height) * 360, 0, 359);
            Redraw();
            $(document).on('mousemove', mmHandler_r);
        } 
        else if(x <= ColorPickerInfo.CV.width * 20 / 24)
        {
            ColorPickerInfo.s = Clamp(x / (20 / 24 * ColorPickerInfo.CV.width), 0, 1);
            ColorPickerInfo.v = Clamp(1 - (y / ColorPickerInfo.CV.height), 0, 1);
            Redraw();
            $(document).on('mousemove', mmHandler_l);
        }

        
    });

    $(document).mouseup(function(e){
        $(document).off('mousemove', mmHandler_r);
        $(document).off('mousemove', mmHandler_l);
    });
}

function Redraw()
{
    // Create a color according to H
    var STEPS = 99;
    var W = ColorPickerInfo.CV.width * 20 / 24;
    var Hd = ColorPickerInfo.CV.height / (STEPS + 1);

    r = HSV2RGB(ColorPickerInfo.h, 1, 1);
    rd = {r: r.r/STEPS, g: r.g/STEPS, b: r.b/STEPS};
    l = {r: 1, g: 1, b: 1};
    ld = {r: 1/STEPS, g: 1/STEPS, b:1/STEPS};

    for(i = 0; i <= STEPS; i++)
    {
        // Create gradient
        gd = ColorPickerInfo.Ctx.createLinearGradient(0, 0, W, 0);
        gd.addColorStop(0, `rgb(${(l.r - ld.r * i)*255}, ${(l.g - ld.g * i)*255}, ${(l.b - ld.b * i)*255})`);
        gd.addColorStop(1, `rgb(${(r.r - rd.r * i)*255}, ${(r.g - rd.g * i)*255}, ${(r.b - rd.b * i)*255})`);
        // Draw
        ColorPickerInfo.Ctx.fillStyle = gd;
        ColorPickerInfo.Ctx.fillRect(0, Hd * i, W, 200);
    }

    // Hue Bar
    gd = ColorPickerInfo.Ctx.createLinearGradient(0, 0, 0, ColorPickerInfo.CV.height);
    gd.addColorStop(0,    "rgb(255,   0,   0)");
    gd.addColorStop(0.84, "rgb(255,   0, 255)");
    gd.addColorStop(0.67, "rgb(0,     0, 255)");
    gd.addColorStop(0.49, "rgb(0,   255, 255)");
    gd.addColorStop(0.33, "rgb(0,   255,   0)");
    gd.addColorStop(0.15, "rgb(255, 255,   0)");
    gd.addColorStop(1,    "rgb(255,   0,   0)");
    ColorPickerInfo.Ctx.fillStyle = gd;
    ColorPickerInfo.Ctx.fillRect(21 / 20 * W, 0, 30, 200);

    // Final Color
    fc = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
    fc = {r: fc.r*255, g: fc.g*255, b:fc.b*255};
    ColorPickerInfo.color = `rgb(${fc.r},${fc.g},${fc.b})`;
    ColorPickerInfo.Pad.style.backgroundColor = ColorPickerInfo.color;

    // Indicator
    // Hue
    ColorPickerInfo.Ctx.fillStyle = 'black';
    ColorPickerInfo.Ctx.fillRect(21 / 20 * W, ColorPickerInfo.h * ColorPickerInfo.CV.height / 360 - 1, 30, 3);
    ColorPickerInfo.Ctx.fillStyle = 'white';
    ColorPickerInfo.Ctx.fillRect(21 / 20 * W, ColorPickerInfo.h * ColorPickerInfo.CV.height / 360, 30, 1);
    // S/V
    ColorPickerInfo.Ctx.fillStyle = 'white';
    ColorPickerInfo.Ctx.beginPath();
    ColorPickerInfo.Ctx.arc(ColorPickerInfo.s * W, (1 - ColorPickerInfo.v) * ColorPickerInfo.CV.height, 7, 0, Math.PI*2, true);
    ColorPickerInfo.Ctx.fill();
    ColorPickerInfo.Ctx.fillStyle = 'black';
    ColorPickerInfo.Ctx.beginPath();
    ColorPickerInfo.Ctx.arc(ColorPickerInfo.s * W, (1 - ColorPickerInfo.v) * ColorPickerInfo.CV.height, 6, 0, Math.PI*2, true);
    ColorPickerInfo.Ctx.fill();
    ColorPickerInfo.Ctx.fillStyle = 'white';
    ColorPickerInfo.Ctx.beginPath();
    ColorPickerInfo.Ctx.arc(ColorPickerInfo.s * W, (1 - ColorPickerInfo.v) * ColorPickerInfo.CV.height, 4, 0, Math.PI*2, true);
    ColorPickerInfo.Ctx.fill();

    ColorPickerInfo.Ctx.fillStyle = ColorPickerInfo.color;
    ColorPickerInfo.Ctx.beginPath();
    ColorPickerInfo.Ctx.arc(ColorPickerInfo.s * W, (1 - ColorPickerInfo.v) * ColorPickerInfo.CV.height, 3, 0, Math.PI*2, true);
    ColorPickerInfo.Ctx.fill();
    
    ColorPickerInfo.Ctx.clearRect(W, 0, W / 20, ColorPickerInfo.CV.height);

    UpdateRGB();
}

function HSV2RGB(h, s, v)
{
    h = Clamp(h, 0, 359);
    s = Clamp(s, 0, 1);
    v = Clamp(v, 0, 1);

    hi = Math.floor(h / 60) % 6;
    f = (h / 60) - hi;
    p = v * (1-s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);

    if(hi == 0) return {r: v, g: t, b: p};
    if(hi == 1) return {r: q, g: v, b: p};
    if(hi == 2) return {r: p, g: v, b: t};
    if(hi == 3) return {r: p, g: q, b: v};
    if(hi == 4) return {r: t, g: p, b: v};
    if(hi == 5) return {r: v, g: p, b: q};
}

function RGB2HSV(r, g, b)
{
    max = Math.max(r, g, b);
    min = Math.min(r, g, b);

    if(max == min) h = 0;
    else if (max == r && g >= b) h = 60 * ((g - b)/(max-min));
    else if (max == r && g < b) h = 60 * ((g - b)/(max-min)) + 360;
    else if (max == g) h = 60 * ((b-r)/(max-min)) + 120;
    else if (max == b) h = 60 * ((r-g)/(max-min)) + 240;


    if(max == 0) s = 0;
    else s = 1 - (min/max);

    v = max/255;

    return {h:h, s:s, v:v};
}

function Clamp(val, min, max)
{
    if(val < min) val = min;
    if(val > max) val = max;
    return val;
}

function UpdateRGB()
{
    c = HSV2RGB(ColorPickerInfo.h, ColorPickerInfo.s, ColorPickerInfo.v);
    ColorPickerInfo.Rval.value = Math.round(c.r * 255);
    ColorPickerInfo.Gval.value = Math.round(c.g * 255);
    ColorPickerInfo.Bval.value = Math.round(c.b * 255);
}

function RGBUpdated()
{
    ColorPickerInfo.Rval.value = Clamp(ColorPickerInfo.Rval.value, 0, 255);
    ColorPickerInfo.Gval.value = Clamp(ColorPickerInfo.Gval.value, 0, 255);
    ColorPickerInfo.Bval.value = Clamp(ColorPickerInfo.Bval.value, 0, 255);
    c = RGB2HSV(ColorPickerInfo.Rval.value, ColorPickerInfo.Gval.value, ColorPickerInfo.Bval.value);
    ColorPickerInfo.h = c.h;
    ColorPickerInfo.s = c.s;
    ColorPickerInfo.v = c.v;
    Redraw();
}

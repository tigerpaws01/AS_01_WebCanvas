const tools = 16;

function ToolHighlight(tool, highlight, check)
{
    if(check && Data.selectedTool == tool.toolOrder) return;
    tool.src = tool.src.substr(0, tool.src.length-5) + (highlight ? 's' : 'u') + ".png";
    tool.style.opacity = (!highlight || Data.selectedTool == tool.toolOrder) ? 1 : 0.5;
}

function ChooseTool(tool)
{
    Data.Select(tool.toolOrder);
    ToolHighlight(tool, true, false);
    ToolUpdate();
}

function ToolSetup()
{
    // For all tools, register with click/hover event
    var toolBar = document.getElementById("toolbar");
    var i = 0;
    for(i = 1; i <= tools; i++)
    {
        var img = document.createElement("img");
        img.toolOrder = i;
        img.src = `img/tools/${i}_u.png`;
        img.className = "non-selectable";
        img.style = "margin: 0px 7px";
        //img.className = "col-sm-1";
        img.style.width = "4%";

        toolBar.appendChild(img);
        if(i == 11 || i == 14 || i == 15) continue;
        
        img.addEventListener('mouseenter', function(e) {
            ToolHighlight(this, true, true);
        });
        img.addEventListener('mouseleave', function(e) {
            ToolHighlight(this, false, true);
        });
        img.addEventListener('mousedown', function() {
            ChooseTool(this);
        })
        
    }

    // Undo
    toolBar.children[13].onmouseenter = function(e){
        console.log("UNDO MDE");
        if(!CanUndo()) return;
        ToolHighlight(this, true, true);
    };
    toolBar.children[13].onmouseleave = function(e){
        console.log("UNDO MDL");
        if(!CanUndo()) return;
        ToolHighlight(this, false, true);
    };
    toolBar.children[13].onmousedown = function(e){
        console.log("UNDO MDD");
        if(!CanUndo()) return;
        ChooseTool(this);
    };
    // Redo
    toolBar.children[14].onmouseenter = function(e){
        console.log("Redo MDE");
        if(!CanRedo()) return;
        ToolHighlight(this, true, true);
    };
    toolBar.children[14].onmouseleave = function(e){
        console.log("Redo MDL");
        if(!CanRedo()) return;
        ToolHighlight(this, false, true);
    };
    toolBar.children[14].onmousedown = function(e){
        console.log("Redo MDD");
        if(!CanRedo()) return;
        ChooseTool(this);
    };

    ToolUpdate();
}

function ToolUpdate()
{
    var tb = document.getElementById("toolbar");
    for(i = 0; i < tools; i++)
    {
        tb.children[i].src = tb.children[i].src.substr(0, tb.children[i].src.length-5) + (Data.selectedTool-1 == i ? 's' : 'u') + ".png";
    }
    // Undo
    tb.children[13].style.opacity = CanUndo() ? 1 : 0.3;
    // Redo
    tb.children[14].style.opacity = CanRedo() ? 1 : 0.3;
}